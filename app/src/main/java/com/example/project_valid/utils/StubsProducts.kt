package com.example.project_valid.utils

import com.example.project_valid.screens.common.model.Product

class StubsProducts {

    fun getListProducts(): ArrayList<Product>{
        val product1 = Product("1", "Zapatillas", "Nike", "N/A", "https://deportesmanzanedo.com/23644/zapatillas-nike-air-max-motion-2-ao0266-005-negra-hombre.jpg")
        val product2 = Product("2", "Camiseta", "Rebook", "10% de descuento", "https://m.media-amazon.com/images/I/51+Mpf7uHaL._AC_UL320_.jpg")
        val product3 = Product("3", "Buzo", "Adidas", "30% de descuento", "https://assets.adidas.com/images/w_600,f_auto,q_auto/d3f1f6f549604a8aa572a833011c045a_9366/Buzo_con_Capucha_Nova_Blanco_CE4801_01_laydown.jpg")
        val product4 = Product("4", "Zapatillas", "Jordan", "15% de descuento", "https://image-cdn.essentiallysports.com/wp-content/uploads/20200519193829/Air-Jordan-3-%E2%80%9CKatrina%E2%80%9D-1-773x432-1.jpg")
        val product5 = Product("5", "Camiseta", "Adidas", "N/A", "https://assets.adidas.com/images/w_600,f_auto,q_auto/c51dd9a5576841059d31a83500d4d926_9366/Camiseta_3_Rayas_Negro_CW1202_01_laydown.jpg")
        val product6 = Product("5", "Jogger", "Nike", "20% de descuento", "https://www.thepoint.es/16326-thickbox_default/nike-sportswear-jogger-gris-blanco.jpg")
        return arrayListOf(product1, product2, product3, product4, product5, product6)
    }
}