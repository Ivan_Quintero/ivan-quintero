package com.example.project_valid.screens.common.model

class Product (
    val id: String?,
    val name: String?,
    val brand: String?,
    var dcto: String?,
    val url: String?
)