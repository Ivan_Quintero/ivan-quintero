package com.example.project_valid.screens.main.repository

import android.content.Context
import com.example.project_valid.BuildConfig
import com.example.project_valid.screens.common.base.BaseRepository
import com.example.project_valid.utils.AppConstants
import com.valid.vssh_android_core.VsshCoreManager
import com.valid.vssh_android_core.model.SetupModel
import javax.inject.Inject
import javax.inject.Singleton

class MainRepository @Inject constructor(private val coreManager: VsshCoreManager) :
    BaseRepository() {

    lateinit var isSetup: (value: Boolean) -> Unit

    fun setUp(context: Context?, isSetup: (value: Boolean) -> Unit) {
        this.isSetup = isSetup

        val setupModel = SetupModel()
        setupModel.context = context
        setupModel.urlBase = BuildConfig.BASE_URL
        setupModel.apiGetCertificates = AppConstants.API_GET_CERTIFICATES
        setupModel.apiExchange = AppConstants.API_EXCHANGE_KEY
        setupModel.publicKey = BuildConfig.PUBLIC_KEY
        coreManager.setup(
            setupModel,
            {
                this.isSetup(it)
            },
            {
                this.isSetup(false)
            }
        )
    }
}
