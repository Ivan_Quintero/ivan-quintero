package com.example.project_valid.screens.common.base

import com.example.project_valid.screens.common.model.VsshCommunicationRequest
import com.valid.communication.events.BaseErrorEvent
import com.valid.communication.events.BaseSuccessEvent
import com.valid.communication.managers.communicationmanager.CommunicationManager
import com.valid.communication.managers.communicationmanager.CommunicationManagerCallback
import com.valid.communication.models.BaseModelResponse

open class BaseRepository {

    lateinit var communicationManager: CommunicationManager

    fun <R : Any> consumeVsshService(
        request: VsshCommunicationRequest,
        responseType: Class<R>,
        response: (BaseModelResponse<in R>) -> Unit,
        error: (BaseErrorEvent) -> Unit
    ) {

        this.communicationManager.setEnableRequestWithSSLPinning(request.sslPinning)
        this.communicationManager.isResponseEncrypted(request.responseEncrypted)
        this.communicationManager.disableSecureRequest(!request.secureRequest)
        this.communicationManager.addSessionIdInEncryptedData(request.addSessionIdToRequest)

        this.communicationManager.setCallback(object : CommunicationManagerCallback {
            override fun errorEvent(errorEvent: BaseErrorEvent?) {
                errorEvent?.let {
                    error(it)
                }
            }

            override fun successEvent(successEvent: BaseSuccessEvent?) {
                successEvent?.let {
                    response(
                        successEvent.getBaseResponseModel(responseType)
                    )
                }
            }
        })

    }
}
