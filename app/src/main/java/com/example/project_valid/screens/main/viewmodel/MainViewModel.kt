package com.example.project_valid.screens.main.viewmodel

import androidx.databinding.ObservableField
import com.example.project_valid.screens.common.base.BaseViewModel
import com.example.project_valid.screens.main.repository.MainRepository
import javax.inject.Inject

class MainViewModel @Inject constructor(private val mainRepository: MainRepository) :
    BaseViewModel() {
    val test = ObservableField<String>()

    fun write() {
        test.set("test")

        mainRepository.setUp(null) {
            //Nothing
        }
    }
}
