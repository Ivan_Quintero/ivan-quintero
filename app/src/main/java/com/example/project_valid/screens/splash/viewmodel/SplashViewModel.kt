package com.example.project_valid.screens.splash.viewmodel

import android.content.Context
import androidx.lifecycle.MutableLiveData
import com.example.project_valid.screens.common.base.BaseViewModel
import com.example.project_valid.screens.common.model.Product
import com.example.project_valid.screens.splash.repository.SplashRepository
import com.example.project_valid.screens.splash.view.ProductListAdapter
import javax.inject.Inject

class SplashViewModel @Inject constructor(private val splashRepository: SplashRepository) :
    BaseViewModel() {

    lateinit var context: Context
    var isSetup: MutableLiveData<Boolean> = MutableLiveData()
    var firebaseIsSetup: MutableLiveData<Boolean> = MutableLiveData()
    var discount: MutableLiveData<String> = MutableLiveData()
    var listProduct: MutableLiveData<ArrayList<Product>> = MutableLiveData()
    private var productListAdapter :  ProductListAdapter? = null

    fun init(context: Context) {
        this.context = context
        splashRepository.setUp(context) {
            isSetup.value = it
        }
    }

    fun initFirebase(){
        splashRepository.setUpFirebase {
            firebaseIsSetup.value = it
        }
    }

    fun getProducts(){
        listProduct.value = splashRepository.getProductsList()
    }

    fun getProductsAdapter(): ProductListAdapter?{
        discount.value = splashRepository.getDiscount()
        productListAdapter = ProductListAdapter(listProduct.value!!, context , discount.value!!)
        return productListAdapter
    }

}
