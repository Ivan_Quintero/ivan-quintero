package com.example.project_valid.screens.main.view

import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import com.example.project_valid.R
import com.example.project_valid.databinding.ActivityMainBinding
import com.example.project_valid.screens.common.base.BaseActivity
import com.example.project_valid.screens.main.viewmodel.MainViewModel

class MainActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val binding: ActivityMainBinding =
            DataBindingUtil.setContentView(this, R.layout.activity_main)

        val viewModel: MainViewModel =
            ViewModelProviders.of(this, viewModelFactory).get(MainViewModel::class.java)
        //binding.viewModel = viewModel

        viewModel.write()
    }
}
