package com.example.project_valid.screens.splash.view

import android.content.Intent
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.example.project_valid.R
import com.example.project_valid.databinding.ActivitySplashBinding
import com.example.project_valid.screens.common.base.BaseActivity
import com.example.project_valid.screens.main.view.MainActivity
import com.example.project_valid.screens.splash.viewmodel.SplashViewModel

class SplashActivity : BaseActivity() {

    private lateinit var viewModel: SplashViewModel
    private val productListAdapter = ProductListAdapter(arrayListOf(), this, "")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val binding: ActivitySplashBinding =
            DataBindingUtil.setContentView(this, R.layout.activity_splash)
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(SplashViewModel::class.java)
        binding.viewModel = viewModel
        binding.lifecycleOwner = this

        viewModel.init(this)
        viewModel.initFirebase()
        viewModel.getProducts()

        observeViewModel()
    }


    private fun observeViewModel() {
        viewModel.isSetup.observe(this, Observer { isSetup ->
            if (isSetup) {
                startActivity(Intent(this, MainActivity::class.java))
            }
        })

        viewModel.listProduct.observe(this, Observer { listProduct ->
            listProduct.let {
                productListAdapter.updateProductList(listProduct)
            }
        })

        viewModel.firebaseIsSetup.observe(this, Observer {firebaseIsSetup->
            if (firebaseIsSetup){
                viewModel.discount.observe(this, Observer {
                })

            }
        })

    }


}
