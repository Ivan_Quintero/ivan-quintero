package com.example.project_valid.screens.splash.view

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.project_valid.R
import com.example.project_valid.databinding.ProductItemBinding
import com.example.project_valid.screens.common.model.Product

class ProductListAdapter(private val productList: ArrayList<Product>, private val context: Context, private val discount: String): RecyclerView.Adapter<ProductListAdapter.ProductViewHolder>() {

    private lateinit var binding: ProductItemBinding
    class ProductViewHolder(var view: View): RecyclerView.ViewHolder(view)

    fun updateProductList(newProductList: List<Product>){
        productList.clear()
        productList.addAll(newProductList)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ProductViewHolder {
        binding = DataBindingUtil.inflate(LayoutInflater.from(parent.context),R.layout.product_item,parent,false)
        return ProductViewHolder(binding.root)
    }



    override fun onBindViewHolder(holder: ProductViewHolder, position: Int) {

        val item = productList[position]
        item.dcto = discount
        binding.productModel = item

        Glide.with(context)
            .load(item.url)
            .into(binding.imageView)
    }


    override fun getItemCount() = productList.size
}