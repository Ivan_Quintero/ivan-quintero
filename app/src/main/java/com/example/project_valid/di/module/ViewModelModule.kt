package com.example.project_valid.di.module

import androidx.lifecycle.ViewModel
import com.example.project_valid.di.annotation.ViewModelKey
import com.example.project_valid.screens.main.viewmodel.MainViewModel
import com.example.project_valid.screens.splash.viewmodel.SplashViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract interface ViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(MainViewModel::class)
    fun bindMainViewModel(viewModel: MainViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(SplashViewModel::class)
    fun bindSplashViewModel(viewModel: SplashViewModel): ViewModel
}
